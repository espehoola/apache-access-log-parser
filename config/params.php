<?php

return [
    'adminEmail' => 'admin@example.com',
    'accessLog' => 'runtime/access.log'
];
