<?php

use yii\db\Migration;

/**
 * Class m181004_020012_table_user
 */
class m181004_020012_table_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
            'expired_at' => $this->integer(),
            'access_token' => $this->string(40),
        ]);

        $row = [
            'username' => 'admin',
            'email' => 'dbahtin@yandex.ru',
            'password_hash' => '$2y$13$CRNSJAyKatzQx1Nj2HCWEeE6P372ccQy.lM8Wiv4rrMtbDUmBvizq',
            'status' => 1
        ];

        Yii::$app->db->createCommand()->insert('user', $row)->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181004_020012_table_user cannot be reverted.\n";

        return false;
    }
}
