<?php

use yii\db\Migration;

/**
 * Class m181004_020023_table_log
 */
class m181004_020023_table_log extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%log}}', [
            'id' => $this->primaryKey(),
            'ip' => $this->string(),
            'identity' => $this->string(),
            'user' => $this->string(),
            'date' => $this->string(),
            'time' => $this->string(),
            'timezone' => $this->string(),
            'timestamp' => $this->integer(),
            'method' => $this->string(),
            'path' => $this->string(),
            'protocol' => $this->string(),
            'status' => $this->string(),
            'bytes' => $this->integer(),
            'referer' => $this->string(),
            'agent' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('log');
    }
}
