<?php

namespace app\models\search;


use app\models\Log;
use yii\data\ActiveDataProvider;

/**
 * Class LogSearch
 * @package app\models\search
 */
class LogSearch extends Log
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['timestamp', 'ip'], 'string'],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Log::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'ip', $this->ip]);

        if ($this->timestamp) {
            $time = explode('-', $this->timestamp);
            $from = strtotime(@$time[0]);
            $query->andFilterWhere(['>=', "timestamp", $from]);
            if (($to = strtotime(@$time[1])) !== false) {
                $query->andFilterWhere(['<=', "timestamp", $to]);
            }
        }

        return $dataProvider;
    }
}