INSTALLATION
------------

```
composer install
php init
php yii migrate
```

CONFIGURATION
-------------
- Edit the file config/main-local.php with real data.
- Edit the file config/params.php with real path to apache access_log

USAGE
-----
<b>Get access_token</b>

method:GET

URL: http://SERVER_NAME/login/access-token

Params:

username: Username or Email from registration

password: Password from registration

<b>Get apache log (JSON)</b>

method: GET

URL: http://SERVER_NAME/log

params:

access_token: access_token

<b>Get apache log with search</b>

method: GET

URL: http://SERVER_NAME/log/search

params:

access_token: access_token

date: dateFrom (format:d.m.Y H:i:s)-dateTo (format:d.m.Y H:i:s)

ip: string

P.S. Be carefull, is used different name of action [access-token] and parameter of request {access_token}