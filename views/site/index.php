<?php

use kartik\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Parser';
$this->params['breadcrumbs'][] = $this->title;
Pjax::begin();
?>

<div class="index">

    <?= GridView::widget([
        'id' => 'a-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax' => true,
        'hover' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Дата',
                'attribute' => 'timestamp',
                'format' => 'datetime',
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'timePicker'=> true,
                        'timePicker24Hour' => true,
                        'locale' => [
                            'format' => 'DD.MM.YYYY HH:mm'
                        ]
                    ],
                ]
            ],

            'ip',

            'agent',

            'method',

            'path',

            'status',

            'protocol',

            'bytes'

        ],
    ]); ?>
</div>
<?php Pjax::end(); ?>
