<?php


namespace app\commands;


use app\models\Log;
use yii\console\Controller;
use Yii;

/**
 * Class ParseController
 * @package app\commands
 */
class ParseController extends Controller
{
    /**
     * @var
     */
    public $file;

    /**
     * @var string
     */
    public $pattern = "/(\S+) (\S+) (\S+) \[([^:]+):(\d+:\d+:\d+) ([^\]]+)\] (\".*?\") (\S+) (\S+) (\".*?\") (\".*?\")/";

    /**
     * Parse access log
     */
    public function actionIndex()
    {
        $method = '';
        $path = '';
        $protocol = '';

        $this->file = fopen(Yii::$app->params['accessLog'], 'r');
        while (($line = fgets($this->file, 4096)) !== false) {
            preg_match ($this->pattern, $line, $result, PREG_UNMATCHED_AS_NULL);

            if (count($result) == 0) {
                continue;
            }

            $date = str_replace('/', ' ', $result[4]);
            $time = $result[5];
            $timestamp = strtotime($date . ' ' . $time);

            $params = explode(' ', $result[7]);

            if (count($params) > 1) {
                $method = $params[0];
                $path = $params[1];
                $protocol = $params[2];
            } else {
                $method = $path = $protocol = $params[0];
            }

            $log = Log::find()
                ->where([
                    'timestamp' => $timestamp,
                    'path' => $path
                ])
                ->one();

            if ($log !== null) {
                continue;
            }

            $log = new Log();
            $log->ip = $result[1];
            $log->identity = $result[2];
            $log->user = $result[3];
            $log->date = $date;
            $log->time = $time;
            $log->timestamp = $timestamp;
            $log->timezone = $result[6];
            $log->method = $method;
            $log->path = $path;
            $log->protocol = $protocol;
            $log->status = $result[8];
            $log->bytes = $result[9];
            $log->referer = $result[10];
            $log->agent = $result[11];
            if (!$log->save()) {
                print_r ($log->errors);
            }
        }
    }
}